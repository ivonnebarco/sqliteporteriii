import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';

import { DatabaseProvider } from '../../providers/database/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  developer = {};
  developers = [];

  constructor(public navCtrl: NavController, private databaseprovider: DatabaseProvider, private platform: Platform) {
    this.databaseprovider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.loadDeveloperData();
      }
    })
  }
 
  //Carga los registros existentes
  loadDeveloperData() {
    this.databaseprovider.getAllDevelopers().then(data => {
      this.developers = data;
    })
  }
 
  //Agrega registros desde el formulario
  addDeveloper() {
    this.databaseprovider.addDeveloper(this.developer['pkidtiposector'], this.developer['codigotiposector'], this.developer['nombretiposector'], this.developer['tiposectoractivo'], this.developer['creaciontiposector'], this.developer['modificaciontiposector'], this.developer['descripciontiposector'])
    .then(data => {
      this.loadDeveloperData();
    });
    this.developer = {};
  }
}
