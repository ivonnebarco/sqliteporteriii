import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;

  constructor(public sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform, private http: Http) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'tiposector.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          this.storage.get('database_filled').then(val => {
            if (val) {
              this.databaseReady.next(true);
            } else {
              this.fillDatabase();
            }
          });
        });
    });
  }
 
  //Llena la base de datos
  fillDatabase() {
    this.http.get('assets/tablaTipoSector.sql')
      .map(res => res.text())
      .subscribe(sql => {
        this.sqlitePorter.importSqlToDb(this.database, sql)
          .then(data => {
            this.databaseReady.next(true);
            this.storage.set('database_filled', true);
            console.log("FIN DE INSERCIÓN");
          })
          .catch(e => console.error(e));
      });
  }
 
  //Inserta registros a la bd
  addDeveloper(pkidtiposector, codigotiposector, nombretiposector, tiposectoractivo, creaciontiposector, modificaciontiposector, descripciontiposector) {
    let data = [pkidtiposector, codigotiposector, nombretiposector, tiposectoractivo, creaciontiposector, modificaciontiposector, descripciontiposector]
    return this.database.executeSql("INSERT INTO tsector (pkidtiposector, codigotiposector, nombretiposector, tiposectoractivo, creaciontiposector, modificaciontiposector, descripciontiposector) VALUES (?, ?, ?, ?, ?, ?, ?)", data).then(data => {
      return data;
    }, err => {
      console.log('Error: ', err);
      return err;
    });
  }
 
  //Selecciona los registros existentes
  getAllDevelopers() {
    return this.database.executeSql("SELECT * FROM tsector", []).then((data) => {
      let developers = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          developers.push({ pkidtiposector: data.rows.item(i).pkidtiposector, codigotiposector: data.rows.item(i).codigotiposector, nombretiposector: data.rows.item(i).nombretiposector, tiposectoractivo: data.rows.item(i).tiposectoractivo, creaciontiposector: data.rows.item(i).creaciontiposector, modificaciontiposector: data.rows.item(i).modificaciontiposector, descripciontiposector: data.rows.item(i).descripciontiposector });
        }
        console.log("N° Registros: " + data.rows.length);
      }
      return developers;
    }, err => {
      console.log('Error: ', err);
      return [];
    });
  }
 
  //Check al estado de la bd
  getDatabaseState() {
    return this.databaseReady.asObservable();
  }}
